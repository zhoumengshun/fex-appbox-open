# Fex UNIAPP工具箱小程序

#### 特别声明
本软件属于技术开源软件, 任何使用本源码从事商业活动，对别人和自己造成损失的，本人概不负责！


#### 技术交流社区

##### 微信群:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ群:
![Image text](doc/wechat_qrcode.jpg) 
![Image text](doc/qq_qrcode.jpg) 

#### 微信小程序
![Image text](https://miniboxs.oss-cn-shenzhen.aliyuncs.com/common/gh_4264e951ad60_344.jpg) 
![Image text](https://miniboxs.oss-cn-shenzhen.aliyuncs.com/common/gh_1fbc78d17b4e_344.png) 
![Image text](https://miniboxs.oss-cn-shenzhen.aliyuncs.com/iq/images/gh_7ea0cd57e928_344.jpg) 
![Image text](https://shkj1994.oss-cn-shenzhen.aliyuncs.com/gh_1c63fd731ccc_430.jpg) 
![Image text](https://smilehert.oss-cn-hongkong.aliyuncs.com/upload/images/0ba2feeb068e4f11af472098c906bcfc.jpg) 
![Image text](https://shkj1994.oss-cn-shenzhen.aliyuncs.com/gh_a79c05053be4_430.jpg) 

#### 功能规划

#### 版本更新

##### 2021-04-12: 新增垃圾分类查询功能

![Image text](doc/1.jpg) 
![Image text](doc/2.jpg) 
![Image text](doc/3.jpg) 
![Image text](doc/4.jpg) 
![Image text](doc/5.jpg) 
![Image text](doc/6.jpg) 
![Image text](doc/7.jpg) 
![Image text](doc/8.jpg) 
![Image text](doc/9.jpg) 
![Image text](doc/10.jpg) 
![Image text](doc/11.jpg)
![Image text](doc/12.jpg)


#### 安装教程

1. 下载HbuilderX
2. 手机USB连接电脑
3. 打开开发者模式, USB调试
4. 打开HbuilderX, 选择 运行 -> 运行到手机或模拟器 -> 选择连接的手机  等待安装

#### 使用说明

1. 将项目拖入[HbuilderX](http://www.dcloud.io/hbuilderx.html) 

#### 参与贡献

暂无

